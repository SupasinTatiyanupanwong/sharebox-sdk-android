package no.sharebox.sdk.tasks;

import android.support.annotation.MainThread;

public abstract class Task<T> {

    private OnResultListener<T> resultListener;
    private OnErrorListener errorListener;

    /**
     * Specifies result listener for this task. Listener will be called from the main thread.
     */
    public Task<T> onResult(OnResultListener<T> listener) {
        this.resultListener = listener;
        return this;
    }

    /**
     * Specifies error listener for this task. Listener will be called from the main thread.
     */
    public Task<T> onError(OnErrorListener listener) {
        this.errorListener = listener;
        return this;
    }

    /**
     * Executes this task and triggers result / error callbacks.
     */
    public abstract TaskRequest subscribe();


    @MainThread
    void deliverResult(T result) {
        if (resultListener != null) {
            resultListener.onResult(result);
        }
    }

    @MainThread
    void deliverError(Exception ex) {
        if (errorListener != null) {
            errorListener.onError(ex);
        }
    }

    void clear() {
        resultListener = null;
        errorListener = null;
    }

}
