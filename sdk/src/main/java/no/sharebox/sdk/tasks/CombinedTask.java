package no.sharebox.sdk.tasks;

public class CombinedTask<T1, T2, R> extends Task<R> {

    private final Task<T1> task1;
    private final Task<T2> task2;
    private Combinator<T1, T2, R> combinator;

    private T1 result1;
    private T2 result2;

    private boolean isFinished1;
    private boolean isFinished2;

    private boolean isError;

    public CombinedTask(Task<T1> task1, Task<T2> task2, Combinator<T1, T2, R> combinator) {
        this.task1 = task1;
        this.task2 = task2;
        this.combinator = combinator;

        task1.onResult(new OnResultListener<T1>() {
            @Override
            public void onResult(T1 result) {
                result1 = result;
                isFinished1 = true;
                deliverResult();
            }
        });
        task2.onResult(new OnResultListener<T2>() {
            @Override
            public void onResult(T2 result) {
                result2 = result;
                isFinished2 = true;
                deliverResult();
            }
        });

        final OnErrorListener errorListener = new OnErrorListener() {
            @Override
            public void onError(Exception ex) {
                if (!isError) {
                    isError = true;
                    deliverError(ex);
                }
            }
        };
        task1.onError(errorListener);
        task2.onError(errorListener);
    }

    private void deliverResult() {
        if (isFinished1 && isFinished2) {
            deliverResult(combinator.combine(result1, result2));
        }
    }

    @Override
    public TaskRequest subscribe() {
        final TaskRequest request1 = task1.subscribe();
        final TaskRequest request2 = task2.subscribe();
        return new TaskRequest(new TaskCancellation() {
            @Override
            public void cancel() {
                request1.cancel();
                request2.cancel();
            }
        });
    }

    public interface Combinator<T1, T2, R> {
        R combine(T1 result1, T2 result2);
    }

}
