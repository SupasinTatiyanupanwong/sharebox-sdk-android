package no.sharebox.sdk.tasks;

public class SubscriptionTask<T> extends Task<T> {

    private final Subscription<T> subscription;

    public SubscriptionTask(Subscription<T> subscription) {
        this.subscription = subscription;
    }

    @Override
    public TaskRequest subscribe() {
        return subscription.subscribe(this);
    }

    @Override
    public Task<T> onError(OnErrorListener listener) {
        throw new UnsupportedOperationException("Subscription task does not propagate any errors, "
                + "you should not set error listener");
    }

}
