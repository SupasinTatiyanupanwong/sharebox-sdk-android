package no.sharebox.sdk;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;

import java.util.UUID;

import no.sharebox.api.domain.UserProfile;

class Preferences {

    private static final String PREFS_NAME = "no.sharebox.sdk.prefs";

    private static final String KEY_DEVICE_ID = "device_id";
    private static final String KEY_SESSION_ID = "session_id";
    private static final String KEY_PHONE_TO_VERIFY = "phone_to_verify";
    private static final String KEY_PHONE_TOKEN = "phone_token";
    private static final String KEY_PROFILE = "profile";

    private final SharedPreferences prefs;

    Preferences(Context context) {
        this.prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }


    String getDeviceId() {
        String deviceId = prefs.getString(KEY_DEVICE_ID, null);
        if (deviceId == null) {
            deviceId = UUID.randomUUID().toString();
            prefs.edit().putString(KEY_DEVICE_ID, deviceId).apply();
        }
        return deviceId;
    }


    void saveSessionId(String sessionId) {
        if (sessionId == null) {
            prefs.edit().remove(KEY_SESSION_ID).apply();
        } else {
            prefs.edit().putString(KEY_SESSION_ID, sessionId).apply();
        }
    }

    String getSessionId() {
        return prefs.getString(KEY_SESSION_ID, null);
    }


    void savePhoneToVerify(String phone) {
        if (phone == null) {
            prefs.edit().remove(KEY_PHONE_TO_VERIFY).apply();
        } else {
            prefs.edit().putString(KEY_PHONE_TO_VERIFY, phone).apply();
        }
    }

    String getPhoneToVerify() {
        return prefs.getString(KEY_PHONE_TO_VERIFY, null);
    }


    void savePhoneToken(String phoneToken) {
        if (phoneToken == null) {
            prefs.edit().remove(KEY_PHONE_TOKEN).apply();
        } else {
            prefs.edit().putString(KEY_PHONE_TOKEN, phoneToken).apply();
        }
    }

    String getPhoneToken() {
        return prefs.getString(KEY_PHONE_TOKEN, null);
    }


    void saveProfile(UserProfile profile) {
        if (profile == null) {
            prefs.edit().remove(KEY_PROFILE).apply();
        } else {
            try {
                String json = ConvertUtils.profileToJson(profile);
                prefs.edit().putString(KEY_PROFILE, json).apply();
            } catch (JSONException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    UserProfile getProfile() {
        String json = prefs.getString(KEY_PROFILE, null);
        if (json == null) {
            return null;
        } else {
            try {
                return ConvertUtils.jsonToProfile(json);
            } catch (JSONException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

}
