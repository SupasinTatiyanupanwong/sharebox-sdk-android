package no.sharebox.sdk;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.domain.UserProfile;

class ConvertUtils {

    private static final String USER_FIRST_NAME = "first_name";
    private static final String USER_LAST_NAME = "last_name";
    private static final String USER_EMAIL = "email";
    private static final String USER_PHONE = "phone";
    private static final String USER_STATUS = "status";
    private static final String USER_CONSENT_GRANTED = "consent_granted";


    private ConvertUtils() {}


    static String profileToJson(@NonNull UserProfile profile) throws JSONException {
        return new JSONObject()
                .put(USER_FIRST_NAME, profile.getFirstName())
                .put(USER_LAST_NAME, profile.getLastName())
                .put(USER_EMAIL, profile.getEmail())
                .put(USER_PHONE, profile.getPhone())
                .put(USER_STATUS, profile.getStatus().name())
                .put(USER_CONSENT_GRANTED, profile.isConsentGranted())
                .toString();
    }

    @NonNull
    static UserProfile jsonToProfile(@NonNull String json) throws JSONException {
        JSONObject obj = new JSONObject(json);

        String statusName = obj.getString(USER_STATUS);
        UserProfile.Status status;
        try {
            status = UserProfile.Status.valueOf(statusName);
        } catch (Exception e) {
            Log.e("Sharebox", "Invalid status name: " + statusName);
            status = UserProfile.Status.OK;
        }

        return new UserProfile.Builder()
                .firstName(obj.getString(USER_FIRST_NAME))
                .lastName(obj.getString(USER_LAST_NAME))
                .email(obj.getString(USER_EMAIL))
                .phone(obj.getString(USER_PHONE))
                .status(status)
                .consentGranted(obj.optBoolean(USER_CONSENT_GRANTED, true))
                .build();
    }

}
