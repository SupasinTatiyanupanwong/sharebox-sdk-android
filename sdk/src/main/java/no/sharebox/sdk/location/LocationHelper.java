package no.sharebox.sdk.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Helper class to facilitate locations update listening.
 * <p>
 * Requires 'com.google.android.gms:play-services-location:X.X.X' dependency in your build script
 * and either {@link Manifest.permission#ACCESS_COARSE_LOCATION} or
 * {@link Manifest.permission#ACCESS_FINE_LOCATION} in your manifest.
 * <p>
 * You will also need to manually call {@link #handlePermissionsResult(int, int[])} from
 * {@link Activity#onRequestPermissionsResult(int, String[], int[])} or
 * {@link Fragment#onRequestPermissionsResult(int, String[], int[])}
 * <p>
 * Location updates should be resumed and paused manually with {@link #resume()} and
 * {@link #pause()} methods. If your target Activity or Fragment implements {@link LifecycleOwner}
 * you can also use {@link #autoConnect()} method instead which will handle resume / pause callbacks
 * automatically.
 */
public class LocationHelper {

    // Some random number, in hope it will not collide with other app's codes
    private static final int LOCATION_REQUEST_CODE = 43982;
    private static final long UPDATE_INTERVAL = 5000L;


    private final Activity activity;
    private final Fragment fragmentCompat;
    private final android.app.Fragment fragment;
    private final OnLocationChangedListener listener;
    private final Lifecycle lifecycle;
    private final FusedLocationProviderClient client;

    private final LocationCallback callback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult result) {
            Location location = result.getLastLocation();
            if (location != null && listener != null) {
                listener.onLocationChanged(location);
            }
        }
    };

    private boolean isPermissionRefused;


    public LocationHelper(Activity activity, OnLocationChangedListener listener) {
        this(activity, null, null, listener);
    }

    @SuppressWarnings("unused") // Public API
    public LocationHelper(Fragment fragment, OnLocationChangedListener listener) {
        this(fragment.getActivity(), fragment, null, listener);
    }

    @SuppressWarnings("unused") // Public API
    public LocationHelper(android.app.Fragment fragment, OnLocationChangedListener listener) {
        this(fragment.getActivity(), null, fragment, listener);
    }

    private LocationHelper(Activity activity, Fragment fragmentCompat,
            android.app.Fragment fragment, OnLocationChangedListener listener) {
        this.activity = activity;
        this.fragmentCompat = fragmentCompat;
        this.fragment = fragment;
        this.listener = listener;

        if (fragmentCompat != null) {
            lifecycle = fragmentCompat.getLifecycle();
        } else if (fragment instanceof LifecycleOwner) {
            lifecycle = ((LifecycleOwner) fragment).getLifecycle();
        } else if (activity instanceof LifecycleOwner) {
            lifecycle = ((LifecycleOwner) activity).getLifecycle();
        } else {
            lifecycle = null;
        }

        this.client = LocationServices.getFusedLocationProviderClient(activity);
    }


    /**
     * Automatically subscribes and un-subscribes from user location updates, based on
     * underlying Activity or Fragment lifecycle.<br>
     *
     * @see #resume()
     * @see #pause()
     */
    public void autoConnect() {
        if (lifecycle == null) {
            throw new IllegalArgumentException("Cannot auto connect to location changes," +
                    " make sure host activity or fragment implements LifecycleOwner");
        }

        lifecycle.addObserver(new LifecycleObserver() {

            @SuppressLint("MissingPermission") // Checked for host method
            @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
            public void onResume() {
                resume();
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
            public void onPause() {
                pause();
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            public void onDestroy() {
                // Un-subscribing
                lifecycle.removeObserver(this);
            }

        });
    }

    /**
     * Subscribes to user location updates.
     */
    @SuppressWarnings("WeakerAccess") // Public API
    public void resume() {
        if (isPermissionRefused) {
            return;
        }

        String[] permissions = new String[] {
                isFinePermissionDeclared(activity) ? ACCESS_FINE_LOCATION : ACCESS_COARSE_LOCATION
        };

        if (fragmentCompat != null) {

            fragmentCompat.requestPermissions(permissions, LOCATION_REQUEST_CODE);

        } else if (fragment != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fragment.requestPermissions(permissions, LOCATION_REQUEST_CODE);
            } else {
                // Manually checking for permission
                int[] result = new int[] {
                        ContextCompat.checkSelfPermission(activity, permissions[0])
                };
                handlePermissionsResult(LOCATION_REQUEST_CODE, result);
            }

        } else {

            ActivityCompat.requestPermissions(activity, permissions, LOCATION_REQUEST_CODE);

        }

        isPermissionRefused = true;
    }

    /**
     * Callback method for permissions grant process, should be called from
     * {@link Activity#onRequestPermissionsResult(int, String[], int[])} or
     * {@link Fragment#onRequestPermissionsResult(int, String[], int[])}.
     */
    public boolean handlePermissionsResult(int requestCode, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocationUpdates();
            } else {
                isPermissionRefused = true;
            }

            return true;
        } else {
            return false;
        }
    }

    @SuppressLint("MissingPermission") // This method is only called if permission is granted
    private void requestLocationUpdates() {
        final LocationRequest request = new LocationRequest()
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(UPDATE_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        client.requestLocationUpdates(request, callback, null);
    }

    /**
     * Un-subscribes from user location updates.
     */
    @SuppressWarnings("WeakerAccess") // Public API
    public void pause() {
        client.removeLocationUpdates(callback);
    }


    private static boolean isFinePermissionDeclared(Context context) {
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);

            if (packageInfo != null && packageInfo.requestedPermissions != null) {
                for (String permission : packageInfo.requestedPermissions) {
                    if (ACCESS_FINE_LOCATION.equals(permission)) {
                        return true;
                    }
                }
            }

            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}
