package no.sharebox.sdk.sample.locations;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import no.sharebox.api.domain.OpenHours;
import no.sharebox.api.domain.StoreLocation;
import no.sharebox.sdk.sample.R;

public class LocationInfoView extends LinearLayout {

    private static final float ZOOM_LEVEL = 17f;

    private final TextView name;
    private final TextView address;
    private final TextView hours;

    public LocationInfoView(Context context) {
        this(context, null);
    }

    public LocationInfoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.location_info_view, this, true);
        setOrientation(VERTICAL);

        this.name = findViewById(R.id.store_name);
        this.address = findViewById(R.id.store_address);
        this.hours = findViewById(R.id.store_hours);
    }

    public void showLocationInfo(StoreLocation location) {
        name.setText(location.getName());
        address.setText(location.getFullAddress());
        hours.setText(formatHours(location));

        Activity activity = (Activity) getContext();
        MapFragment map = (MapFragment) activity.getFragmentManager().findFragmentById(R.id.map);
        map.getMapAsync(googleMap -> onMapReady(googleMap, location));
    }

    private String formatHours(StoreLocation location) {
        OpenHours hours = location.findOpenHoursToday();
        if (hours == null) {
            return getResources().getString(R.string.store_closed);
        } else if (hours.is24Hours()) {
            return getResources().getString(R.string.store_open_24);
        } else {
            return hours.getTimeRange();
        }
    }

    private void onMapReady(GoogleMap map, StoreLocation location) {
        map.getUiSettings().setAllGesturesEnabled(false);

        LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
        map.addMarker(new MarkerOptions().position(point).title(location.getName()));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, ZOOM_LEVEL));
    }

}
