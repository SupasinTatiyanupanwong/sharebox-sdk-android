package no.sharebox.sdk.sample.locations;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import java.util.List;

import no.sharebox.api.domain.StoreLocation;
import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.location.LocationHelper;
import no.sharebox.sdk.sample.ErrorsHandler;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;

public class LocationsListActivity extends BaseActivity {

    private LocationsAdapter adapter;
    private boolean isLoading;

    private LocationHelper locationHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.locations_activity);
        getSupportActionBarNotNull().setDisplayHomeAsUpEnabled(true);

        RecyclerView list = findViewById(R.id.locations);
        list.setLayoutManager(new LinearLayoutManager(this));
        adapter = new LocationsAdapter();
        list.setAdapter(adapter);

        loadLocations();

        locationHelper = new LocationHelper(this, this::onUserLocationChanged);
        locationHelper.autoConnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isLoading) {
            addProgress(menu);
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        boolean handled = locationHelper.handlePermissionsResult(requestCode, grantResults);

        if (!handled) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void onUserLocationChanged(@NonNull Location location) {
        adapter.setUserLocation(location);
    }


    private void loadLocations() {
        isLoading = true;
        invalidateOptionsMenu();

        ShareboxSdk.get().getLocations()
                .onResult(this::onLocationsLoaded)
                .onError(this::onLocationsError)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onLocationsLoaded(List<StoreLocation> locations) {
        isLoading = false;
        invalidateOptionsMenu();

        adapter.setLocations(locations);
    }

    private void onLocationsError(Exception ex) {
        isLoading = false;
        invalidateOptionsMenu();

        ErrorsHandler.handle(this, ex);
    }

}
