package no.sharebox.sdk.sample.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import no.sharebox.api.ShareboxApi;
import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.ShareboxUtils;
import no.sharebox.sdk.sample.ErrorsHandler;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;
import no.sharebox.sdk.sample.consent.ConsentActivity;
import no.sharebox.sdk.sample.menu.MenuActivity;
import no.sharebox.sdk.sample.registration.RegistrationActivity;

public class LoginActivity extends BaseActivity {

    private static final int CONSENT_REQUEST_CODE = 1;

    private ViewHolder views;

    private boolean isRequestingCode;
    private boolean isLoggingIn;

    private int consentVersion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        views = new ViewHolder(this);

        views.consentRow.setOnClickListener(view -> askForConsent());
        views.request.setOnClickListener(view -> requestCode());
        views.requestAgain.setOnClickListener(view -> requestCode());
        views.verify.setOnClickListener(view -> login());

        updateUiState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONSENT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                views.consent.setChecked(true);
                consentVersion = ConsentActivity.getVersion(data);
                updateUiState();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isRequestingCode || isLoggingIn) {
            addProgress(menu);
        }
        return true;
    }


    /* ------------------------- */
    /* Consent                   */
    /* ------------------------- */

    private void askForConsent() {
        if (ShareboxSdk.get().isVerificationRequested()) {
            // Cannot change decision if we already requested verification
            return;
        }

        if (views.consent.isChecked()) {
            // Just un-checking the checkbox
            views.consent.setChecked(false);
            consentVersion = 0;
            updateUiState();
        } else {
            // Showing consent activity
            startActivityForResult(new Intent(this, ConsentActivity.class), CONSENT_REQUEST_CODE);
        }
    }


    /* ------------------------- */
    /* SMS code verification     */
    /* ------------------------- */

    private void requestCode() {
        String phone = views.phone.getText().toString();

        if (TextUtils.isEmpty(phone)) {
            views.phone.setError(getString(R.string.error_required_field));
            return;
        } else if (!ShareboxUtils.isValidPhoneFormat(phone)) {
            views.phone.setError(getString(R.string.error_invalid_phone));
            return;
        } else if (consentVersion == 0) {
            // Consent should be granted at this point
            return;
        }

        isRequestingCode = true;
        updateUiState();

        ShareboxSdk.get().verifyPhone(phone, consentVersion)
                .onResult(result -> onCodeRequestSuccess())
                .onError(this::onCodeRequestFailed)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onCodeRequestSuccess() {
        isRequestingCode = false;
        updateUiState();

        Toast.makeText(this, R.string.message_code_sent, Toast.LENGTH_SHORT).show();

        // Switching to code field
        views.code.requestFocus();
        InputMethodManager manager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (manager != null) {
            manager.showSoftInput(views.code, 0);
        }
    }

    private void onCodeRequestFailed(Exception ex) {
        isRequestingCode = false;
        updateUiState();

        ErrorsHandler.handle(this, ex);
    }


    /* ------------------------- */
    /* Login                     */
    /* ------------------------- */

    private void login() {
        String code = views.code.getText().toString();

        if (TextUtils.isEmpty(code)) {
            views.code.setError(getString(R.string.error_required_field));
            return;
        }

        isLoggingIn = true;
        updateUiState();

        ShareboxSdk.get().login(code)
                .onResult(result -> onLoginSuccess())
                .onError(this::onLoginFailed)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onLoginSuccess() {
        isLoggingIn = false;
        updateUiState();

        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }

    private void onLoginFailed(Exception ex) {
        isLoggingIn = false;
        updateUiState();

        if (ErrorsHandler.isApiError(ex, ShareboxApi.ERROR_NOT_REGISTERED)) {
            startActivity(new Intent(this, RegistrationActivity.class));
            finish();
        } else {
            ErrorsHandler.handle(this, ex);
        }
    }


    /* ------------------------- */
    /* UI state                  */
    /* ------------------------- */

    private void updateUiState() {
        invalidateOptionsMenu();

        final boolean requested = ShareboxSdk.get().isVerificationRequested();
        final boolean consentGranted = views.consent.isChecked();

        views.consent.setEnabled(!requested && !isRequestingCode);
        views.request.setEnabled(!isRequestingCode && consentGranted);
        views.requestAgain.setEnabled(!isRequestingCode);
        views.verify.setEnabled(!isLoggingIn);

        views.code.setVisibility(requested ? View.VISIBLE : View.GONE);
        views.request.setVisibility(requested ? View.GONE : View.VISIBLE);
        views.requestAgain.setVisibility(requested ? View.VISIBLE : View.GONE);
        views.verify.setVisibility(requested ? View.VISIBLE : View.GONE);

        if (requested) {
            views.phone.setText(ShareboxSdk.get().getPhoneToVerify());
        }
    }


    private static class ViewHolder {
        final EditText phone;
        final EditText code;
        final View consentRow;
        final CheckBox consent;
        final View request;
        final View requestAgain;
        final View verify;

        ViewHolder(Activity activity) {
            this.phone = activity.findViewById(R.id.phone);
            this.code = activity.findViewById(R.id.code);
            this.consentRow = activity.findViewById(R.id.consent_row);
            this.consent = activity.findViewById(R.id.consent);
            this.request = activity.findViewById(R.id.request_code);
            this.requestAgain = activity.findViewById(R.id.request_code_again);
            this.verify = activity.findViewById(R.id.verify);
        }
    }

}
