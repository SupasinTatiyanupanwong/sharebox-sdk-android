package no.sharebox.api;

import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.List;

import no.sharebox.api.domain.Consent;
import no.sharebox.api.domain.Reservation;
import no.sharebox.api.domain.StoreLocation;
import no.sharebox.api.domain.UserProfile;
import no.sharebox.api.exceptions.ShareboxApiException;
import no.sharebox.api.exceptions.ShareboxException;
import no.sharebox.api.exceptions.ShareboxHttpException;
import no.sharebox.api.methods.ApiMethod;
import no.sharebox.api.methods.GetConsentMethod;
import no.sharebox.api.methods.GetLocationMethod;
import no.sharebox.api.methods.GetLocationsMethod;
import no.sharebox.api.methods.GetProfileMethod;
import no.sharebox.api.methods.GetReservationMethod;
import no.sharebox.api.methods.GetReservationsHistoryMethod;
import no.sharebox.api.methods.GetReservationsMethod;
import no.sharebox.api.methods.GrantConsentMethod;
import no.sharebox.api.methods.LoginMethod;
import no.sharebox.api.methods.OpenLockerMethod;
import no.sharebox.api.methods.RegisterMethod;
import no.sharebox.api.methods.ReserveLockerMethod;
import no.sharebox.api.methods.StopSubscriptionMethod;
import no.sharebox.api.methods.VerifyPhoneMethod;
import no.sharebox.api.methods.VerifySmsCodeMethod;
import no.sharebox.api.methods.requests.GetLocationParams;
import no.sharebox.api.methods.requests.GetReservationParams;
import no.sharebox.api.methods.requests.GrantConsentParams;
import no.sharebox.api.methods.requests.LoginParams;
import no.sharebox.api.methods.requests.OpenLockerParams;
import no.sharebox.api.methods.requests.RegisterParams;
import no.sharebox.api.methods.requests.ReserveLockerParams;
import no.sharebox.api.methods.requests.StopSubscriptionParams;
import no.sharebox.api.methods.requests.VerifyPhoneParams;
import no.sharebox.api.methods.requests.VerifySmsCodeParams;
import no.sharebox.api.methods.responses.GetReservationData;
import no.sharebox.api.methods.responses.LoginData;
import no.sharebox.api.methods.responses.RegisterData;
import no.sharebox.api.methods.responses.ReserveLockerData;
import no.sharebox.api.methods.responses.VerifySmsCodeData;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.http.HttpMethod;
import okhttp3.logging.HttpLoggingInterceptor;

public class ShareboxApi {

    public static final int ERROR_NOT_REGISTERED = 3;
    public static final int ERROR_INVALID_SMS_CODE = 401;
    public static final int ERROR_TOO_FAR_FROM_LOCKER = 105;

    private static final String DEFAULT_BASE_URL = "https://prod.sharebox.no:3000";

    private static ShareboxApi instance;

    private final String baseUrl;
    private final String authHeader;
    private final OkHttpClient client;

    private ShareboxApi(Options options) {
        // Initializing base url
        if (options.baseUrl == null || options.baseUrl.length() == 0) {
            baseUrl = DEFAULT_BASE_URL;
        } else if (options.baseUrl.endsWith("/")) {
            baseUrl = options.baseUrl.substring(0, options.baseUrl.length() - 1);
        } else {
            baseUrl = options.baseUrl;
        }

        if (options.authLogin == null || options.authPass == null) {
            throw new ShareboxException(
                    "Missing API credentials, use \"Options.credentials(String, String)\" method.");
        } else {
            authHeader = Credentials.basic(options.authLogin, options.authPass);
        }

        // Initializing HTTP client
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (options.debug) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }

        client = builder.build();
    }

    public static ShareboxApi init(Options options) {
        return instance = new ShareboxApi(options);
    }

    @SuppressWarnings("unused") // Public API
    public static ShareboxApi get() {
        if (instance == null) {
            throw new NullPointerException(
                    "You need to initialize ShareboxApi with \"init(Options)\" method");
        }
        return instance;
    }


    /**
     * Requests latest consent info.
     */
    public Consent getConsent() throws IOException {
        return execute(new GetConsentMethod(), null, null);
    }

    /**
     * Requests phone verification code to be sent by SMS.
     */
    public void verifyPhone(VerifyPhoneParams params) throws IOException {
        execute(new VerifyPhoneMethod(), params, null);
    }

    /**
     * Verifies phone number by SMS code as requested by {@link #verifyPhone(VerifyPhoneParams)}
     * method.
     */
    public VerifySmsCodeData verifySmsCode(VerifySmsCodeParams params) throws IOException {
        return execute(new VerifySmsCodeMethod(), params, null);
    }

    /**
     * Logs user in. Throws {@link ShareboxApiException} with error code
     * {@link #ERROR_NOT_REGISTERED} if user is not yet registered.
     */
    public LoginData login(LoginParams params) throws IOException {
        return execute(new LoginMethod(), params, null);
    }

    /**
     * Registers new user profile.
     */
    public RegisterData register(RegisterParams params) throws IOException {
        return execute(new RegisterMethod(), params, null);
    }

    /**
     * Returns profile of currently logged in user.
     */
    public UserProfile getProfile(String sessionId) throws IOException {
        return execute(new GetProfileMethod(), null, sessionId);
    }

    /**
     * Saves information about latest granted consent version for current user.
     */
    public void grantConsent(String sessionId, GrantConsentParams params) throws IOException {
        execute(new GrantConsentMethod(), params, sessionId);
    }

    /**
     * Returns list of store locations.
     */
    public List<StoreLocation> getLocations(String sessionId) throws IOException {
        return execute(new GetLocationsMethod(), null, sessionId);
    }

    /**
     * Returns store location details, including cabinets and their availability.
     */
    public StoreLocation getLocation(String sessionId, GetLocationParams params)
            throws IOException {
        return execute(new GetLocationMethod(), params, sessionId);
    }

    /**
     * Reserves a locker.
     */
    public ReserveLockerData reserveLocker(String sessionId, ReserveLockerParams params)
            throws IOException {
        return execute(new ReserveLockerMethod(), params, sessionId);
    }

    /**
     * Returns list of user's active reservations.
     */
    public List<Reservation> getReservations(String sessionId) throws IOException {
        return execute(new GetReservationsMethod(), null, sessionId);
    }

    /**
     * Returns list of recently finished user's reservations.
     */
    public List<Reservation> getReservationsHistory(String sessionId) throws IOException {
        return execute(new GetReservationsHistoryMethod(), null, sessionId);
    }

    /**
     * Returns reservation details.
     */
    public GetReservationData getReservation(String sessionId, GetReservationParams params)
            throws IOException {
        return execute(new GetReservationMethod(), params, sessionId);
    }

    /**
     * Stops a subscription.
     */
    public void stopSubscription(String sessionId, StopSubscriptionParams params)
            throws IOException {
        execute(new StopSubscriptionMethod(), params, sessionId);
    }

    /**
     * Opens a locker. Throws {@link ShareboxApiException} with error code
     * {@link #ERROR_TOO_FAR_FROM_LOCKER} if API determines that the user is far from the locker.
     * See {@link OpenLockerParams} for corresponding method params.
     */
    public void openLocker(String sessionId, OpenLockerParams params) throws IOException {
        execute(new OpenLockerMethod(), params, sessionId);
    }


    private <I, O> O execute(ApiMethod<I, O> method, @Nullable I params, @Nullable String sessionId)
            throws IOException {

        RequestBody body = HttpMethod.permitsRequestBody(method.getMethod())
                ? method.createBody(params, sessionId) : null;

        Request request = new Request.Builder()
                .url(baseUrl + method.getPath())
                .addHeader("Authorization", authHeader)
                .method(method.getMethod(), body)
                .build();

        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            return method.parseBody(response.body());
        } else {
            throw new ShareboxHttpException(response.code());
        }
    }


    public static class Options {

        String baseUrl;
        boolean debug;
        String authLogin;
        String authPass;

        public Options baseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
            return this;
        }

        /**
         * Sets credentials to access API. <b>Required.</b>
         */
        public Options credentials(String login, String password) {
            this.authLogin = login;
            this.authPass = password;
            return this;
        }

        /**
         * @param debug Whether to log API requests and responses.
         */
        public Options debugLogs(boolean debug) {
            this.debug = debug;
            return this;
        }

    }

}
