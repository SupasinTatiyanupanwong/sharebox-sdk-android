package no.sharebox.api.exceptions;

public class ShareboxException extends RuntimeException {

    public ShareboxException(String message) {
        super(message);
    }

    public ShareboxException(String message, Throwable cause) {
        super(message, cause);
    }

}
