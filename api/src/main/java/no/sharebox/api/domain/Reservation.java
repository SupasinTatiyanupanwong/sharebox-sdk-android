package no.sharebox.api.domain;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import no.sharebox.api.ShareboxApi;
import no.sharebox.api.methods.requests.GetReservationParams;

@SuppressWarnings("unused") // Public API
public class Reservation {

    final long id;
    final long date;
    final long subscriptionId;
    final String name;
    final boolean isFromYou;
    final ReservationUser owner;
    final List<ReservationUser> recipients;
    final int recipientsCount;
    final StoreLocation location;
    final long lockerId;
    final int lockerNumber;
    final Type type;
    final boolean isActive;
    final boolean isOpen;
    final int freeHours;

    Reservation(Builder builder) {
        this.id = builder.id;
        this.date = builder.date;
        this.subscriptionId = builder.subscriptionId;
        this.name = builder.name;
        this.isFromYou = builder.isFromYou;
        this.owner = builder.owner;
        this.recipients = builder.recipients == null
                ? Collections.<ReservationUser>emptyList()
                : Collections.unmodifiableList(builder.recipients);
        this.recipientsCount = builder.recipientsCount;
        this.location = builder.location;
        this.lockerId = builder.lockerId;
        this.lockerNumber = builder.lockerNumber;
        this.type = builder.type;
        this.isActive = builder.isActive;
        this.isOpen = builder.isOpen;
        this.freeHours = builder.freeHours;
    }

    public long getId() {
        return id;
    }

    public long getDate() {
        return date;
    }

    public long getSubscriptionId() {
        return subscriptionId;
    }

    public String getName() {
        return name;
    }

    public boolean isFromYou() {
        return isFromYou;
    }

    public ReservationUser getOwner() {
        return owner;
    }

    /**
     * Recipients list.
     * <p>
     * Note, that this can be an incomplete list. Full recipients list can be retrieved using
     * {@link ShareboxApi#getReservation(String, GetReservationParams) reservation details API}.<br>
     * But {@link #getRecipientsCount()} method will always return total number of recipients.
     */
    @NonNull
    public List<ReservationUser> getRecipients() {
        return recipients;
    }

    @SuppressWarnings("WeakerAccess") // Public API
    public int getRecipientsCount() {
        return recipientsCount;
    }

    public StoreLocation getLocation() {
        return location;
    }

    public long getLockerId() {
        return lockerId;
    }

    public int getLockerNumber() {
        return lockerNumber;
    }

    public Type getType() {
        return type;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isOpen() {
        return isOpen;
    }

    /**
     * Number of total free hours available for this reservation.<br>
     * Only for reservations of type {@link Type#FREE}.
     */
    public int getFreeHours() {
        return freeHours;
    }


    /**
     * Whether current user can open the locker from this reservation or not.
     */
    public boolean canOpen() {
        return isActive;
    }

    /**
     * Whether current user can manually finish this reservation or not.
     */
    public boolean canFinish() {
        return isActive && isFromYou && type != Type.MONTHLY;
    }


    @Override
    public boolean equals(Object obj) {
        return this == obj
                || (obj != null && getClass() == obj.getClass() && id == ((Reservation) obj).id);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }


    public static class Builder {

        long id;
        long date;
        long subscriptionId;
        String name;
        boolean isFromYou;
        ReservationUser owner;
        List<ReservationUser> recipients;
        int recipientsCount;
        StoreLocation location;
        long lockerId;
        int lockerNumber;
        Type type;
        boolean isActive;
        boolean isOpen;
        int freeHours;

        public Builder() {}

        public Builder(Reservation reservation) {
            this.id = reservation.id;
            this.date = reservation.date;
            this.subscriptionId = reservation.subscriptionId;
            this.name = reservation.name;
            this.isFromYou = reservation.isFromYou;
            this.owner = reservation.owner;
            this.recipients = reservation.recipients;
            this.recipientsCount = reservation.recipientsCount;
            this.location = reservation.location;
            this.lockerId = reservation.lockerId;
            this.lockerNumber = reservation.lockerNumber;
            this.type = reservation.type;
            this.isActive = reservation.isActive;
            this.isOpen = reservation.isOpen;
            this.freeHours = reservation.freeHours;
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder date(long date) {
            this.date = date;
            return this;
        }

        public Builder subscriptionId(long subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder fromYou(boolean fromYou) {
            this.isFromYou = fromYou;
            return this;
        }

        public Builder owner(ReservationUser owner) {
            this.owner = owner;
            return this;
        }

        public Builder recipients(List<ReservationUser> recipients) {
            this.recipients = recipients;
            return this;
        }

        public Builder recipientsCount(int recipientsCount) {
            this.recipientsCount = recipientsCount;
            return this;
        }

        public Builder location(StoreLocation location) {
            this.location = location;
            return this;
        }

        public Builder lockerId(long lockerId) {
            this.lockerId = lockerId;
            return this;
        }

        public Builder lockerNumber(int lockerNumber) {
            this.lockerNumber = lockerNumber;
            return this;
        }

        public Builder type(Type type) {
            this.type = type;
            return this;
        }

        public Builder active(boolean active) {
            this.isActive = active;
            return this;
        }

        public Builder open(boolean open) {
            this.isOpen = open;
            return this;
        }

        public Builder freeHours(int freeHours) {
            this.freeHours = freeHours;
            return this;
        }

        public Reservation build() {
            return new Reservation(this);
        }

    }


    public enum Type {
        FREE, REGULAR, MONTHLY
    }

}
