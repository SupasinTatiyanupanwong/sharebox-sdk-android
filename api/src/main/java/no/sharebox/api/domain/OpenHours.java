package no.sharebox.api.domain;

import java.util.Calendar;
import java.util.Locale;

@SuppressWarnings({ "WeakerAccess", "unused" }) // Public API
public class OpenHours {

    final int weekday;
    final int startHour;
    final int startMinute;
    final int endHour;
    final int endMinute;

    OpenHours(Builder builder) {
        this.weekday = builder.weekday;
        this.startHour = builder.startHour;
        this.startMinute = builder.startMinute;
        this.endHour = builder.endHour;
        this.endMinute = builder.endMinute;
    }

    /**
     * Weekday number as used in {@link Calendar} constants (Sunday is 1, Monday is 2 and so on).
     */
    public int getWeekday() {
        return weekday;
    }

    public int getStartHour() {
        return startHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public int getEndHour() {
        return endHour;
    }

    public int getEndMinute() {
        return endMinute;
    }


    /**
     * @return true if store is open 24 hours this day, false otherwise.
     */
    public boolean is24Hours() {
        return startHour == endHour && startMinute == endMinute;
    }

    /**
     * Returns formatted time range.
     */
    public String getTimeRange() {
        return String.format(Locale.getDefault(),
                "%02d:%02d - %02d:%02d", startHour, startMinute, endHour, endMinute);
    }


    public static class Builder {

        int weekday;
        int startHour;
        int startMinute;
        int endHour;
        int endMinute;

        public Builder() {}

        public Builder(OpenHours hours) {
            this.weekday = hours.weekday;
            this.startHour = hours.startHour;
            this.startMinute = hours.startMinute;
            this.endHour = hours.endHour;
            this.endMinute = hours.endMinute;
        }

        public Builder weekday(int weekday) {
            this.weekday = weekday;
            return this;
        }

        public Builder start(int hour, int minute) {
            this.startHour = hour;
            this.startMinute = minute;
            return this;
        }

        public Builder end(int hour, int minute) {
            this.endHour = hour;
            this.endMinute = minute;
            return this;
        }

        public OpenHours build() {
            return new OpenHours(this);
        }

    }

}
