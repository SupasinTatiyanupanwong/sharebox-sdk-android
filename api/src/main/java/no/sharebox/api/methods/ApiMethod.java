package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public interface ApiMethod<I, O> {

    String getMethod();

    String getPath();

    @NonNull
    RequestBody createBody(@Nullable I params, @Nullable String sessionId) throws IOException;

    @Nullable
    O parseBody(@Nullable ResponseBody body) throws IOException;

}
