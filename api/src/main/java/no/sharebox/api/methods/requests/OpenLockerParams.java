package no.sharebox.api.methods.requests;

public class OpenLockerParams {

    private final long lockerId;
    private final double latitude;
    private final double longitude;
    private final boolean forceOpen;
    private final boolean finishReservation;

    OpenLockerParams(Builder builder) {
        this.lockerId = builder.lockerId;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.forceOpen = builder.forceOpen;
        this.finishReservation = builder.finishReservation;
    }

    public long getLockerId() {
        return lockerId;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public boolean isForceOpen() {
        return forceOpen;
    }

    public boolean isFinishReservation() {
        return finishReservation;
    }


    public static class Builder {

        long lockerId;
        double latitude;
        double longitude;
        boolean forceOpen;
        boolean finishReservation = true;

        /**
         * @param lockerId <b>Required.</b> Id of a locker to be opened.
         */
        public Builder lockerId(long lockerId) {
            this.lockerId = lockerId;
            return this;
        }

        /**
         * Specifies user's location needed to check if the user is close enough to the locker to
         * open it.
         * <p>
         * If user is far from the locker then it will not be opened and API will return
         * an error. This behavior can be overridden with {@link #forceOpen(boolean)} method.
         *
         * @param latitude Latitude of user's location.
         * @param longitude Longitude of user's location.
         */
        public Builder location(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
            return this;
        }

        /**
         * @param forceOpen Whether the locker should be opened even if the user is not close
         * enough.
         * <p>
         * Default value is false meaning that API will return an error if user is far
         * from the locker, as determined by provided user's coordinates.
         * <p>
         * If this value is set to true then user coordinates can be omitted.
         */
        public Builder forceOpen(boolean forceOpen) {
            this.forceOpen = forceOpen;
            return this;
        }

        /**
         * @param finishReservation Whether corresponding reservation should be finished once the
         * locker is opened.
         * <p>
         * Default value is true meaning that the reservation will be finished.
         * <p>
         * Note: only owner can set this value to false to keep the reservation.
         * Setting it to false from recipient side will be ignored.
         */
        public Builder finishReservation(boolean finishReservation) {
            this.finishReservation = finishReservation;
            return this;
        }

        public OpenLockerParams build() {
            return new OpenLockerParams(this);
        }

    }

}
