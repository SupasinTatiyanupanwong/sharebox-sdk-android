package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.List;

import no.sharebox.api.domain.Reservation;
import no.sharebox.api.methods.converters.ReservationConverter;

public class GetReservationsMethod extends ApiJsonMethod<Void, List<Reservation>> {

    @Override
    public String getPath() {
        return "/v2/sharebox_get_active_reservations";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull Void params) throws JSONException {
        // No extra params
    }

    @Nullable
    @Override
    List<Reservation> parseBody(@NonNull JSONObject json) throws JSONException, ParseException {
        return ReservationConverter.convertList(json.getJSONArray("reservations"));
    }

}
