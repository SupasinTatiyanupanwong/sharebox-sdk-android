package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.domain.StoreLocation;
import no.sharebox.api.methods.converters.StoreLocationConverter;
import no.sharebox.api.methods.requests.GetLocationParams;

public class GetLocationMethod extends ApiJsonMethod<GetLocationParams, StoreLocation> {

    @Override
    public String getPath() {
        return "/v2/sharebox_get_location_details";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull GetLocationParams params)
            throws JSONException {
        json.put("location_id", params.getLocationId());
    }

    @Nullable
    @Override
    StoreLocation parseBody(@NonNull JSONObject json) throws JSONException {
        return StoreLocationConverter.convertSingle(json.getJSONObject("location"), true);
    }

}
