package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.methods.requests.VerifySmsCodeParams;
import no.sharebox.api.methods.responses.VerifySmsCodeData;

public class VerifySmsCodeMethod extends ApiJsonMethod<VerifySmsCodeParams, VerifySmsCodeData> {

    @Override
    public String getPath() {
        return "/v2/sharebox_authenticate_sms_code";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull VerifySmsCodeParams params)
            throws JSONException {
        json.put("mobile", params.getPhone());
        json.put("device_id", params.getDeviceId());
        json.put("sms_code", params.getCode());
    }

    @Nullable
    @Override
    VerifySmsCodeData parseBody(@NonNull JSONObject json) throws JSONException {
        String phoneToken = json.getString("digits_id");
        return new VerifySmsCodeData(phoneToken);
    }

}
