package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.methods.requests.RegisterParams;
import no.sharebox.api.methods.responses.RegisterData;

public class RegisterMethod extends ApiJsonMethod<RegisterParams, RegisterData> {

    @Override
    public String getPath() {
        return "/v2/sharebox_register";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull RegisterParams params) throws JSONException {
        json.put("digits_id", params.getPhoneToken());
        json.put("first_name", params.getFirstName());
        json.put("last_name", params.getLastName());
        json.put("email", params.getEmail());
    }

    @Nullable
    @Override
    RegisterData parseBody(@NonNull JSONObject json) throws JSONException {
        String sessionId = json.getString("session_id"); // Must be there
        return new RegisterData(sessionId);
    }

}
