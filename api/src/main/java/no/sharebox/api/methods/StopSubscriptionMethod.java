package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.methods.requests.StopSubscriptionParams;

public class StopSubscriptionMethod extends ApiJsonMethod<StopSubscriptionParams, Void> {

    @Override
    public String getPath() {
        return "/sharebox_subscription_stop";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull StopSubscriptionParams params)
            throws JSONException {
        json.put("subscription_id", params.getSubscriptionId());
    }

    @Nullable
    @Override
    Void parseBody(@NonNull JSONObject json) throws JSONException {
        return null;
    }

}
