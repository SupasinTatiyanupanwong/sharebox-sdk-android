package no.sharebox.api.methods.requests;

import no.sharebox.api.domain.StoreLocation;

public class ReserveLockerParams {

    private final long cabinetId;
    private final String receiverPhone;
    private final String reservationName;

    ReserveLockerParams(Builder builder) {
        this.cabinetId = builder.cabinetId;
        this.receiverPhone = builder.receiverPhone;
        this.reservationName = builder.reservationName;
    }

    public long getCabinetId() {
        return cabinetId;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public String getReservationName() {
        return reservationName;
    }


    public static class Builder {

        long cabinetId;
        String receiverPhone;
        String reservationName;

        /**
         * @param cabinetId <b>Required.</b> Any available cabinet id as received within locations
         * list (see {@link StoreLocation#findAvailableCabinet()}).
         */
        public Builder cabinetId(long cabinetId) {
            this.cabinetId = cabinetId;
            return this;
        }

        /**
         * @param receiverPhone <b>Required.</b> Receiver's phone number.
         */
        public Builder receiverPhone(String receiverPhone) {
            this.receiverPhone = receiverPhone;
            return this;
        }

        /**
         * @param reservationName <b>Required.</b> Reservation name.
         */
        public Builder reservationName(String reservationName) {
            this.reservationName = reservationName;
            return this;
        }

        public ReserveLockerParams build() {
            return new ReserveLockerParams(this);
        }

    }

}
